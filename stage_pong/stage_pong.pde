int deplacementX, deplacementY;
int ballX, ballY;
int w, z, z2;
int rgbshift;
int colorRect;
int points;
int scoreL, scoreR;
int typeGame;
boolean displayMenu = true;
int scoreSingle;
boolean gameOver = false;

void setup(){
  size(400,400);
  background(4,6,51);
  stroke(255);
  line(200,0,200,400);
   ballX = 200;
   ballY = 200; 
   if(typeGame == 2){
     deplacementX = 3;
     deplacementY = -2;
   } 
   else {
     deplacementX = 5;
     deplacementY = -3;
   }
   w = 15;
   z = 120;
   z2= 120;
   colorRect=0;
   rgbshift=1;
}


void draw(){
  if(displayMenu){
    designMenu();
    
  }
  else if(gameOver){
    endSinglePlayer();
  }
  else {
    design();
    move();
    checkCollisionScreen();
    checkCollisionPaddle();
    scoreCal();
    if(typeGame == 2){
      if (deplacementX == 0 && deplacementY == 0 && (scoreL < 5 && scoreR < 5)){
        text("Press 'r' to start" ,90,200);  
      } 
    }
  }  
} 

void keyPressed() {
  println(keyCode);
  if (key == CODED) {
    if (keyCode == UP) {
      z2=z2-35;
    }
    else if (keyCode == DOWN) {
      z2=z2+35;
    } 
  }
  if (key == 'z' || key == 'Z'){
    z=z-35;
  }
  if (key == 's' || key == 'S') {
    z=z+35;
  } 
  if(keyCode == 82){
    if(typeGame == 2){
      deplacementX = 3;
      deplacementY = -2;
    }
  }
  if(keyCode == 1){
    displayMenu = true;
    designMenu();
  }
  if(keyCode == 49) {
      typeGame = 1;
      displayMenu = false;
      scoreSingle = 0;
      z = 150;
      z2 = 150;
      ballToTheCenter();
  }
  if(keyCode == 50) {
    typeGame = 2;
    displayMenu = false;
    scoreL = 0;
    scoreR = 0;
    z = 150;
    z2 = 150;
    ballToTheCenter();
  }
  if(typeGame == 2 && keyCode == 32){
    ballToTheCenter();
    z = 150;
    z2 = 150;
    deplacementX = 3;
    deplacementY = -2;
    if(scoreL > 0){
      ballToTheCenter();
      z = 150;
      z2 = 150;
      deplacementX = 3;
      deplacementY = -2;
    }
    if(scoreR > 0){
      ballToTheCenter();
      z = 150;
      z2 = 150;
      deplacementX = 3;
      deplacementY = -2;
    }
  } 
  else if(keyCode == 32){
    ballToTheCenter();
    z = 150;
    deplacementX = 5;
    deplacementY = -3;
    scoreSingle = 0;
    gameOver = false;
  }
}

void move(){
  ballX=ballX+deplacementX;
  ballY=ballY+deplacementY;
}
void gradiant(){
  colorRect=colorRect+rgbshift;
    
  if (colorRect == 255) {
    rgbshift=-1;
  }
  if (colorRect == 0) {
    rgbshift=1;
  }
}
 
void checkCollisionScreen (){
  
  if(typeGame == 2){
    if (ballY <= 0 && deplacementY < 0){
      deplacementY = -deplacementY;
    }
    if (ballY > height-10 && deplacementY > 0) 
    { 
      deplacementY = -deplacementY;
    }
  }

  if(typeGame == 1){
    if (ballY <= 0 && deplacementY < 0){
      deplacementY = -deplacementY;
    }
    if (ballY > height-10 && deplacementY > 0){ 
      deplacementY = -deplacementY;
    }
    if (ballX > width-10 && deplacementX > 0){
      deplacementX = -deplacementX;
    }
    if (ballX < 10){
      ballToTheCenter();
      z = 150;
      gameOver = true;
    }
  }
}
    
  
void checkCollisionPaddle(){
  if(typeGame == 1){
    if (ballX < 50+15 && deplacementX < 0 && ballY>z && ballY<z+85){
      deplacementX = -deplacementX;
      scoreSingle = scoreSingle + 1;
    }
  } 
  if(typeGame == 2){
    if (ballX < 50+15 && deplacementX < 0 && ballY>z && ballY<z+85){
      deplacementX = -deplacementX;
    }  
    if (ballX > width-50 && deplacementX > 0 && ballY>z2 && ballY<z2+85){
      deplacementX = -deplacementX;
    }
  }
}

void design (){
  if (typeGame == 2) {
    background(4,6,51);
    stroke(255);
    line(200,0,200,400);
    fill(colorRect);
    rect(25,z,25,85);
    fill(colorRect,colorRect,255);
    stroke (255);
    smooth();
    ellipse(ballX,ballY,30,30);
    fill(colorRect);
    rect(width-50,z2,25,85);
    gradiant();
    texte();
    endScoreMultiplayer();
  }
 else if (typeGame == 1){
    background(4,6,51);
    stroke(255);
    line(200,0,200,400);
    fill(colorRect);
    rect(25,z,25,85);
    fill(colorRect,colorRect,255);
    stroke (255);
    smooth();
    ellipse(ballX,ballY,30,30);
    gradiant();
    texte();
  }
}
void texte(){
  
  if(typeGame == 2){
    fill(255);
    textSize(20);
    text(scoreL,30,30);
    text(scoreR,width-30,30);
  }
  if(typeGame == 1){
    fill(255);
    textSize(24);
    text(scoreSingle,30,30);
  }
}

void scoreCal(){
  
  if(typeGame == 2){   
    if (ballX < 10){
      scoreR = scoreR + 1;
      ballToTheCenter();
     }
    if (ballX > 400 ){
      scoreL = scoreL + 1;
      ballToTheCenter();
    }
  }
}


void ballToTheCenter() {
  deplacementX = 0;
  deplacementY = 0;
  ballX =200;
  ballY = 200;
}

void endScoreMultiplayer(){
  if(typeGame == 2){
    if(scoreR == 5){
        ballToTheCenter();
        println("GAME OVER");
        text("Player 2 won!",130,200);
    }
    if(scoreL == 5){
        ballToTheCenter();
        println("GAME OVER");
        text("Player 1 won!",130,200);
    }
  }
}
void designMenu(){
  size (400,400);
  background(0);
  textSize(36);
  fill(255,96,96);
  text("Welcome to Pong",45,50);
  textSize(20);
  text("(Made by Gabriel Faye)",90,80);
  singleplayer();
  multiplayer();
}

void singleplayer(){
  textSize(24);
  fill(255, 96, 96);
  text("To play Singleplayer press '1'",30,150);
  textSize(22);
  text("(soon...)",155,180);
}

void multiplayer(){
  textSize(24);
  fill(255, 96, 96);
  text("To play Multiplayer press '2'",30,250);
}
  
void endSinglePlayer(){
  println("GAME OVER");
  textSize(32);
  fill(165,0,0);
  text("GAME OVER",105,200);
}
